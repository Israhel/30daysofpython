# --- Day3
# Ejercicios

# %%
# 1. Declare your age as integer variable
my_age = 31

# %%
# 2. Declare your height as a float variable
my_height = 1.71

# %%
# 3. Declare a variable that store a complex number
my_complex = 3 + 3j

# %%
# 4. Write a script that prompts the user to enter base and height of the triangle and calculate an area of this triangle (area = 0.5 x b x h).
base = int(input("Ingresa la base: "))
height = int(input("Ingresa la altura: "))

print(f"El área del triángulo es: {(base * height) / 2}")

# %%
# 5. Write a script that prompts the user to enter side a, side b, and side c of the triangle. Calculate the perimeter of the triangle (perimeter = a + b + c).
side_a = int(input("Ingresa el lado A: "))
side_b = int(input("Ingresa el lado B: "))
side_c = int(input("Ingresa el lado B: "))

print(f"El perímetro del triángulo es: {side_a + side_b + side_c}")

# %%
# 6. Get length and width of a rectangle using prompt. Calculate its area (area = length x width) and perimeter (perimeter = 2 x (length + width))
length = int(input("Ingresa la base: "))
width = int(input("Ingresa la altura: "))

print(f"El área del rectángulo es: {length * width} y su perímetro es: {(length + width) * 2}")

# %%
# 7. Get radius of a circle using prompt. Calculate the area (area = pi x r x r) and circumference (c = 2 x pi x r) where pi = 3.14.
radius = int(input("Ingresa el radio de un círculo: "))

print(f"El área del círculo es: {3.1415962 * radius ** 2} y su circunferencia es: {2 * 3.1415962 * radius}")
# %%
# 8. Slope is (m = y2-y1/x2-x1). Find the slope and Euclidean distance between point (2, 2) and point (6,10)
import math
p = (2, 2)
q = (6, 10)
slope = (q[1] - p[1]) / (q[0] - p[0])
euc_distance = math.sqrt((q[0] - p[0]) ** 2 + (q[1] - p[1]) ** 2)

print(f"La pendiente de los puntos (10, 6) y (2, 2) es: {slope}")
print(f"Y la distacia entre los puntos es: {euc_distance}")

# %%
# 9. Calculate the value of y (y = x^2 + 6x + 9). Try to use different x values and figure out at what x value y is going to be 0.
for x in range(-10, 10):
    print(f"y = {(x ** 2) + (6 * x) + 9}")
    if ((x ** 2) + (6 * x) + 9) == 0:
        print(f"El valor de x donde y = 0 es : {x}")
        break
# %%
# 10. Find the length of 'python' and 'dragon' and make a falsy comparison statement.
py_len = len("python")
dr_len = len("dragon")

print(f"Python y Dragon tienen la misma longitud?: {py_len == dr_len}")
# %%
# 11. Use and operator to check if 'on' is found in both 'python' and 'dragon'
resultado = 'on' in 'python' and 'on' in 'dragon'
print(f"La palabra \"on\" está en Python y Dragon?: {resultado}")
# %%
# 12. I hope this course is not full of jargon. Use in operator to check if jargon is in the sentence.
sentence = "I hope this course is not full of jargon"
to_find = "jargon"

print(f"La palabra 'jargon' está en la frase \n'{sentence}'?: {to_find in sentence}")
# %%
# 13. There is no 'on' in both dragon and python
print(f"No se encuentra la palabra 'on' en 'Python y Dragon'?: {'on' not in 'Python' and 'on' not in 'Dragon'}")
# %%
# 14. Find the length of the text python and convert the value to float and convert it to string
length = len("python")
print(length, type(length))
length = float(length)
print(length,type(length))
length = str(length)
print(length, type(length))
# %%
# 15. Even numbers are divisible by 2 and the remainder is zero. How do you check if a number is even or not using python?
number = int(input("Dame un número: "))

print(f"El número {number} es par") if number % 2 == 0 else print(f"El número {number} no es par")

if number % 2 == 0:
    print(f"El número {number} es par")
else:
    print(f"El número {number} no es par")
# %%
# 16. Check if the floor division of 7 by 3 is equal to the int converted value of 2.7.
floor_division = 7 // 3
int_convert = int(2.7)

print(f"La division entera de 7 / 3 es igual al valor entero de 2.7?: {floor_division == int_convert}")
# %%
# 17. Check if type of '10' is equal to type of 10
print(f"El tipo de dato de '10' es igual que el de 10?: {type('10') == type(10)}")
# %%
# 18. Check if int('9.8') is equal to 10
# convert_to_int = int('9.8') #ValueError: invalid literal for int() with base 10: '9.8'
# print(f"'9.8' es igual a 10?: {convert_to_int == 10}")
# %%
# 19. Writ a script that prompts the user to enter hours and rate per hour. Calculate pay of the person?
horas = int(input("Ingresa las horas trabajadas a la semana: "))
sueldo_hora = int(input("Ingresa el sueldo por hora: "))

print(f"Tu sueldo semanal es de: {horas * sueldo_hora}")
# %%
# 20. Write a script that prompts the user to enter number of years. Calculate the number of seconds a person can live. Assume a person can live hundred years
years = int(input("Cuantos años tienes?: "))

print(f"Has vivido por {years * 365 * 24 * 60 * 60} segundos")
# %%
# 21. Write a Python script that displays the following table
# 1 1 1 1 1
# 2 1 2 4 8
# 3 1 3 9 27
# 4 1 4 16 64
# 5 1 5 25 125
tabla = """
1 1 1 1 1
2 1 2 4 8
3 1 3 9 27
4 1 4 16 64
5 1 5 25 125
"""

print(tabla)

number = 1
resultado = 1

for _ in range(5):
    print(number, resultado, end=" ")
    for _ in range(3):
        resultado *= number
        print(resultado, end=" ")
    print()
    number += 1
    resultado = 1
# %%
