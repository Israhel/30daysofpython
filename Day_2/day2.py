# --- Day 2
# Variables, Builtin Functions

# Exercises Level 1

# 1. Inside 30DaysOfPython create a folder called day_2. Inside this folder create a file named variables.py

# 2. Write a python comment saying 'Day 2: 30 Days of python programming'
# Day 2: 30 Days of python programming

# 3. Declare a first name variable and assign a value to it
first_name = "Israhel"

# 4. Declare a last name variable and assign a value to it
last_name = "Guzman"

# 5. Declare a full name variable and assign a value to it
full_name = first_name + last_name

# 6. Declare a country variable and assign a value to it
country = "Mexico"

# 7. Declare a city variable and assign a value to it
city = "Gdl"

# 8. Declare an age variable and assign a value to it
age = 32

# 9. Declare a year variable and assign a value to it
year = 1990

# 10. Declare a variable is_married and assign a value to it
is_married = True

# 11. Declare a variable is_true and assign a value to it
is_true = False

# 12. Declare a variable is_light_on and assign a value to it
is_light_on = True

# 13. Declare multiple variable on one line
favorit_color, favorit_music, language, prgramming_language = "green", "Reggae", "Spanish", "Python"

# Exercises Level 2

# 1. Check the data print(type of all your variables using print(type() built-in function
print(type(first_name))
print(type(last_name))
print(type(full_name))
print(type(country))
print(type(city))
print(type(age))
print(type(year))
print(type(is_married))
print(type(is_true))
print(type(is_light_on))
print(type(favorit_color))
print(type(favorit_music))
print(type(language))
print(type(prgramming_language))

# 2. Using the len() built-in function, find the length of your first name
print("First name:", first_name)
print("First name length:", len(first_name))

# 3. Compare the length of your first name and your last name
print('First name:', first_name)
print('First name length:', len(first_name))
print('Last name:', last_name)
print('Last name length:', len(last_name))

# 4. Declare 5 as num_one and 4 as num_two
num_one = 5
num_two = 4
print(f"Num 1: {num_one}")
print(f"Num 2: {num_two}")

#     4.1. Add num_one and num_two and assign the value to a variable total
total = num_one + num_two
print(f"Suma: {total}")

#     4.2. Subtract num_two from num_one and assign the value to a variable diff
diff = num_two - num_one
print(f"Resta: {diff}")

#     4.3. Multiply num_two and num_one and assign the value to a variable product
product = num_one * num_two
print(f"Multiplicacion: {product}")

#     4.4. Divide num_one by num_two and assign the value to a variable division
division = num_one / num_two
print(f"Division: {division}")

#     4.5. Use modulus division to find num_two divided by num_one and assign the value to a variable remainder
remainder = num_two % num_one
print(f"Modulo: {remainder}")

#     4.6. Calculate num_one to the power of num_two and assign the value to a variable exp
exp = num_one ** num_two
print(f"Exponente: {exp}")

#     4.7. Find floor division of num_one by num_two and assign the value to a variable floor_division
floor_division = num_one // num_two
print(f"Division entera: {floor_division}")

# 5. The radius of a circle is 30 meters.
radius = 30
print(f"Radio: {radius}")

#     5.1. Calculate the area of a circle and assign the value to a variable name of area_of_circle
area_of_circle = 3.141592 * radius ** 2
print(f"Area del circulo: {area_of_circle}")

#     5.2. Calculate the circumference of a circle and assign the value to a variable name of circum_of_circle
circum_of_circle = 2 * 3.141592 * radius
print(f"Perimetro: {circum_of_circle}")

#     5.4. Take radius as user input and calculate the area.
radius = int(input("Cual es el radio del circulo?: "))
area_of_circle = 3.141592 * radius ** 2
print(f"Area del circulo: {area_of_circle}")

# 6. Use the built-in input function to get first name, last name, country and age from a user and store the value to their corresponding variable names
firs_name = input("Nombre: ")
last_name = input("Apellido: ")
country = input("Pais de origen: ")
age = input("Edad: ")

print(f"Hola {firs_name} {last_name} de {country} de {age} anios")
# 7. Run help('keywords') in Python shell or in your file to check for the Python reserved words or keywords
# help("keywords")
