# --- Day 1
# Basic Operators

print(2 + 3)    # addition (+)
print(3 - 1)    # subtraction (-)
print(5 * 7)    # multiplication (*)
print(10 / 2)   # division (/)
print(3 ** 2)   # exponential (**)
print(19 % 5)   # modulus (%)
print(19 // 5)   # floor division Operator (//)

# Chacking data types

print(10, type(10))                     # Int
print(3.14, type(3.14))                   # Float
print(1 + 3j, type(1 + 3j))                 # Complex number
print('Israhel', type('Israhel'))              # String
print([1, 2, 3], type([1, 2, 3]))              # List
print({'name': 'Israhel'}, type({'name': 'Israhel'}))    # Dictionary
print({9.8, 3.14, 7}, type({9.8, 3.14, 7}))         # Set
print((9.8, 2, 'Israhel'), type((9.8, 2, 'Israhel')))    # Tuple

# Find an Euclidean Distance between (2, 3) and (10, 8)

import math
p = (2, 3)
q = (10, 8)
euclidean_distance = math.sqrt(((p[0] - q[0]) ** 2) + ((p[1] - q[1]) ** 2))
print(euclidean_distance)
