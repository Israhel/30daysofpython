# --- Day 4
# Ejercicios

# %%
# 1. Concatenate the string 'Thirty', 'Days', 'Of', 'Python' to a single string, 'Thirty Days Of Python'.
a = 'Thirty'
b = 'Days'
c = 'Of'
d = 'Python'
one_str = f"{a} {b} {c} {d}"
print(one_str)
# %%
# 2. Concatenate the string 'Coding', 'For' , 'All' to a single string, 'Coding For All'.
a = 'Coding'
b = 'For'
c = 'All'
one_str = f'{a} {b} {c}'
print(one_str)

# %%
# 3. Declare a variable named company and assign it to an initial value "Coding For All".
company = 'Coding For All'

# %%
# 4. Print the variable company using print().
print(company)
# %%
# 5. Print the length of the company string using len() method and print().
print(len(company))
# %%
# 6. Change all the characters to uppercase letters using upper() method.
company = company.upper()
print(company)
# %%
# 7. Change all the characters to lowercase letters using lower() method.
company = company.lower()
print(company)
# %%
# 8. Use capitalize(), title(), swapcase() methods to format the value of the string Coding For All.
company = company.capitalize()
print(company)

company = company.title()
print(company)

company = company.swapcase()
print(company)
# %%
# 9. Cut(slice) out the first word of Coding For All string.
print('Coding For All'[1:])
# %%
# 10. Check if Coding For All string contains a word Coding using the method index, find or other methods.
print('Coding For All'.find('Coding'))
# %%
# 11. Replace the word coding in the string 'Coding For All' to Python.
print('Coding For All'.replace('Coding', 'Python'))
# %%
# 12. Change Python for Everyone to Python for All using the replace method or other methods.
print('Python for Everyone'.replace('Everyone', 'All'))
# %%
# 13. Split the string 'Coding For All' using space as the separator (split()) .
print('Coding For All'.split())
# %%
# 14. "Facebook, Google, Microsoft, Apple, IBM, Oracle, Amazon" split the string at the comma.
print("Facebook, Google, Microsoft, Apple, IBM, Oracle, Amazon".split(', '))
# %%
# 15. What is the character at index 0 in the string Coding For All.
print('Coding For All'[0])
# %%
# 16. What is the last index of the string Coding For All.
print(len('Coding For All') - 1)
# %%
# 17. What character is at index 11 in "Coding For All" string.
print('Coding For All'[11])
# %%
# 18. Create an acronym or an abbreviation for the name 'Python For Everyone'.
print('Python For Everyone'.replace('Python', 'Py').replace('For', '4'))

# %%
# 19. Create an acronym or an abbreviation for the name 'Coding For All'.
print('Coding For All'.replace('Coding', 'Cdng').replace('For', '4'))

# %%
# 20. Use index to determine the position of the first occurrence of C in Coding For All.
print('Coding For All'.index('C'))

# %%
# 21. Use index to determine the position of the first occurrence of F in Coding For All.
print('Coding For All'.index('F'))

# %%
# 22. Use rfind to determine the position of the last occurrence of l in Coding For All People.
print('Coding For All People'.rfind('l'))

# %%
# 23. Use index or find to find the position of the first occurrence of the word 'because' in the following sentence: 'You cannot end a sentence with because because because is a conjunction'
print('You cannot end a sentence with because because because is a conjunction'.find('because'))

# %%
# 24. Use rindex to find the position of the last occurrence of the word because in the following sentence: 'You cannot end a sentence with because because because is a conjunction'
print('You cannot end a sentence with because because because is a conjunction'.rindex('because'))

# %%
# 25. Slice out the phrase 'because because because' in the following sentence: 'You cannot end a sentence with because because because is a conjunction'
print('You cannot end a sentence with because because because is a conjunction'.split('because'))

# %%
# 26. Find the position of the first occurrence of the word 'because' in the following sentence: 'You cannot end a sentence with because because because is a conjunction'
print('You cannot end a sentence with because because because is a conjunction'.index('because'))

# %%
# 27. Slice out the phrase 'because because because' in the following sentence: 'You cannot end a sentence with because because because is a conjunction'
print('You cannot end a sentence with because because because is a conjunction'.split('because because because'))

# %%
# 28. Does ''Coding For All' start with a substring Coding?
print('Coding for All'.startswith('Coding'))

# %%
# 29. Does 'Coding For All' end with a substring coding?
print('Coding for All'.endswith('coding'))

# %%
# 30. '   Coding For All      '  , remove the left and right trailing spaces in the given string.
print('   Coding For All      '.strip())

# %%
# 31. Which one of the following variables return True when we use the method isidentifier():
#      30DaysOfPython
#      thirty_days_of_python
print('30DaysOfPython'.isidentifier()) # Returns False
print('thirty_days_of_python'.isidentifier()) # Returns True

# %%
# 32. The following list contains the names of some of python libraries: ['Django', 'Flask', 'Bottle', 'Pyramid', 'Falcon']. Join the list with a hash with space string.
python_libraries = ['Django', 'Flask', 'Bottle', 'Pyramid', 'Falcon']
result = '# '.join(python_libraries)
print(result)

# %%
# 33. Use the new line escape sequence to separate the following sentences.
#  I am enjoying this challenge.
#  I just wonder what is next.
print('I am enjoying this challenge.\nI just wonder what is next.')

# %%
# 34. Use a tab escape sequence to write the following lines.
#  Name      Age     Country   City
#  Asabeneh  250     Finland   Helsinki
print('Name\tAge\tCountry\tCity'.expandtabs(10))
print('Israhel\t3000\tFinland\tGotham'.expandtabs(10))

# %%
# 35. Use the string formatting method to display the following:
# 
# radius = 10
# area = 3.14 * radius ** 2
# The area of a circle with radius 10 is 314 meters square.
radius = 10
area = 3.14 * radius ** 2
print(f'El área del círculo con radio {radius} es {area:.0f} metros cuadrados')

# %%
# 36. Make the following using string formatting methods:
# 
# 8 + 6 = 14
print(f'8 + 6 = {8 + 6}')

# 8 - 6 = 2
print(f'8 - 6 = {8 - 6}')

# 8 * 6 = 48
print(f'8 * 6 = {8 * 6}')

# 8 / 6 = 1.33
print(f'8 / 6 = {(8 / 6):.2f}')

# 8 % 6 = 2
print(f'8 % 6 = {8 % 6}')

# 8 // 6 = 1
print(f'8 // 6 = {8 // 6}')

# 8 ** 6 = 262144
print(f'8 ** 6 = {8 ** 6}')
# %%
