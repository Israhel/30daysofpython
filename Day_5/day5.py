# --- Day4
# Listas
# %%
# Hay dos formas de crear una lista

# Usando la función incorporada (built-in) de lista
lst = list()
print(lst, len(lst))

# Usando corchetes []
lst = []
print(lst, len(lst))
# %%

# Usamos len() para encontrar la longitud de una lista.
fruits = ['banana', 'orange', 'mango', 'lemon']                     # list of fruits
vegetables = ['Tomato', 'Potato', 'Cabbage','Onion', 'Carrot']      # list of vegetables
animal_products = ['milk', 'meat', 'butter', 'yoghurt']             # list of animal products
web_techs = ['HTML', 'CSS', 'JS', 'React','Redux', 'Node', 'MongDB'] # list of web technologies
countries = ['Finland', 'Estonia', 'Denmark', 'Sweden', 'Norway']

# Print the lists and its length
print('Fruits:', fruits)
print('Number of fruits:', len(fruits))
print('Vegetables:', vegetables)
print('Number of vegetables:', len(vegetables))
print('Animal products:',animal_products)
print('Number of animal products:', len(animal_products))
print('Web technologies:', web_techs)
print('Number of web technologies:', len(web_techs))
print('Countries:', countries)
print('Number of countries:', len(countries))
# %%

# Las listas pueden tener elementos de diferentes tipos de datos
lst = ['Asabeneh', 250, True, {'country':'Finland', 'city':'Helsinki'}]
print(lst)
# %%

# Acceder a los elementos de la lista mediante la indexación positiva
# We access each item in a list using their index. A list index starts from 0.
fruits = ['banana', 'orange', 'mango', 'lemon']
first_fruit = fruits[0] # we are accessing the first item using its index
print(first_fruit)      # banana
second_fruit = fruits[1]
print(second_fruit)     # orange
last_fruit = fruits[3]
print(last_fruit) # lemon
# Last index
last_index = len(fruits) - 1
last_fruit = fruits[last_index]
# %%

# Acceder a los elementos de la lista mediante la indexación negativa
# La indexación negativa significa comenzar desde el final, -1 se refiere al último elemento, -2 se refiere al penúltimo elemento.
fruits = ['banana', 'orange', 'mango', 'lemon']
first_fruit = fruits[-4]
last_fruit = fruits[-1]
second_last = fruits[-2]
print(first_fruit)      # banana
print(last_fruit)       # lemon
print(second_last)      # mango
# %%

# Desempaquetar elementos de la lista
lst = ['item','item2','item3', 'item4', 'item5']
first_item, second_item, third_item, *rest = lst
print(first_item)     # item1
print(second_item)    # item2
print(third_item)     # item3
print(rest)           # ['item4', 'item5']
# %%
# First Example
fruits = ['banana', 'orange', 'mango', 'lemon','lime','apple']
first_fruit, second_fruit, third_fruit, *rest = fruits
print(first_fruit)     # banana
print(second_fruit)    # orange
print(third_fruit)     # mango
print(rest)           # ['lemon','lime','apple']
# Second Example about unpacking list
first, second, third,*rest, tenth = [1,2,3,4,5,6,7,8,9,10]
print(first)          # 1
print(second)         # 2
print(third)          # 3
print(rest)           # [4,5,6,7,8,9]
print(tenth)          # 10
# Third Example about unpacking list
countries = ['Germany', 'France','Belgium','Sweden','Denmark','Finland','Norway','Iceland','Estonia']
gr, fr, bg, sw, *scandic, es = countries
print(gr)
print(fr)
print(bg)
print(sw)
print(scandic)
print(es)
# %%

# Comprobación de un elemento si es miembro de una lista mediante el operador in.
fruits = ['banana', 'orange', 'mango', 'lemon']
does_exist = 'banana' in fruits
print(does_exist)  # True
does_exist = 'lime' in fruits
print(does_exist)  # False
# %%

"""
Para agregar elementos al final de una lista existente, usamos el método append().
# syntax
lst = list()
lst.append(item)
 """
fruits = ['banana', 'orange', 'mango', 'lemon']
fruits.append('apple')
print(fruits)           # ['banana', 'orange', 'mango', 'lemon', 'apple']
fruits.append('lime')   # ['banana', 'orange', 'mango', 'lemon', 'apple', 'lime']
print(fruits)
# %%

"""
Podemos usar el método insert() para insertar un solo elemento 
en un índice específico en una lista.
Tenga en cuenta que otros elementos se desplazan a la derecha.
El método insert() toma dos argumentos: índice y un elemento para insertar.
# syntax
lst = ['item1', 'item2']
lst.insert(index, item)
"""
fruits = ['banana', 'orange', 'mango', 'lemon']
fruits.insert(2, 'apple') # insert apple between orange and mango
print(fruits)           # ['banana', 'orange', 'apple', 'mango', 'lemon']
fruits.insert(3, 'lime')   # ['banana', 'orange', 'apple', 'lime', 'mango', 'lemon']
print(fruits)
# %%

"""
El método remove() elimina un elemento específico de una lista.
Este método elimina la primera aparición del elemento en la lista.
# syntax
lst = ['item1', 'item2']
lst.remove(item)
"""
fruits = ['banana', 'orange', 'mango', 'lemon', 'banana']
fruits.remove('banana')
print(fruits)  # ['orange', 'mango', 'lemon', 'banana']
fruits.remove('lemon')
print(fruits)  # ['orange', 'mango', 'banana']
# %%

"""
El método pop() elimina el índice especificado (o el último elemento si no se especifica el índice)
# syntax
lst = ['item1', 'item2']
lst.pop()       # ultimo elemento
lst.pop(index)
"""
fruits = ['banana', 'orange', 'mango', 'lemon']
fruits.pop()
print(fruits)       # ['banana', 'orange', 'mango']

fruits.pop(0)
print(fruits)       # ['orange', 'mango']
# %%

"""
La palabra clave del elimina el índice especificado y también se puede usar para
eliminar elementos dentro del rango del índice.
También puede eliminar la lista por completo.
# syntax
lst = ['item1', 'item2']
del lst[index] # un solo elemento
del lst        # para borrar la lista por completo
"""
fruits = ['banana', 'orange', 'mango', 'lemon', 'kiwi', 'lime']
del fruits[0]
print(fruits)       # ['orange', 'mango', 'lemon', 'kiwi', 'lime']
del fruits[1]
print(fruits)       # ['orange', 'lemon', 'kiwi', 'lime']
del fruits[1:3]     # this deletes items between given indexes, so it does not delete the item with index 3!
print(fruits)       # ['orange', 'lime']
del fruits
print(fruits)       # This should give: NameError: name 'fruits' is not defined
# %%

"""
El método clear() vacía la lista
# syntax
lst = ['item1', 'item2']
lst.clear()
"""
fruits = ['banana', 'orange', 'mango', 'lemon']
fruits.clear()
print(fruits)       # []
# %%

"""
Es posible copiar una lista reasignándola a una nueva variable
de la siguiente forma: lista2 = lista1. Ahora, list2 es una
referencia de list1, cualquier cambio que hagamos en list2
también modificará el original, list1. Pero hay muchos casos en
los que no queremos modificar el original sino que queremos
tener una copia diferente. Una forma de evitar el problema
anterior es usar copy().
# syntax
lst = ['item1', 'item2']
lst_copy = lst.copy()
"""
fruits = ['banana', 'orange', 'mango', 'lemon']
fruits_copy = fruits.copy()
print(fruits_copy)
# %%

# Hay varias formas de unir o concatenar dos o más listas en Python
# Operador más (+)
positive_numbers = [1, 2, 3, 4, 5]
zero = [0]
negative_numbers = [-5,-4,-3,-2,-1]
integers = negative_numbers + zero + positive_numbers
print(integers)
fruits = ['banana', 'orange', 'mango', 'lemon']
vegetables = ['Tomato', 'Potato', 'Cabbage', 'Onion', 'Carrot']
fruits_and_vegetables = fruits + vegetables
print(fruits_and_vegetables )
# %%
""" 
# Unirlas usando el método extend(). El método extend() permite agregar una lista en una lista.
# syntax
list1 = ['item1', 'item2']
list2 = ['item3', 'item4', 'item5']
list1.extend(list2)
"""
num1 = [0, 1, 2, 3]
num2= [4, 5, 6]
num1.extend(num2)
print('Numbers:', num1) # Numbers: [0, 1, 2, 3, 4, 5, 6]
negative_numbers = [-5,-4,-3,-2,-1]
positive_numbers = [1, 2, 3,4,5]
zero = [0]

negative_numbers.extend(zero)
negative_numbers.extend(positive_numbers)
print('Integers:', negative_numbers)
fruits = ['banana', 'orange', 'mango', 'lemon']
vegetables = ['Tomato', 'Potato', 'Cabbage', 'Onion', 'Carrot']
fruits.extend(vegetables)
print('Fruits and vegetables:', fruits )
# %%

""" 
El método count() devuelve el número de veces que aparece un elemento en una lista
# syntax
lst = ['item1', 'item2']
lst.count(item)
 """
fruits = ['banana', 'orange', 'mango', 'lemon']
print(fruits.count('orange'))   # 1
ages = [22, 19, 24, 25, 26, 24, 25, 24]
print(ages.count(24))           # 3
# %%

""" 
El método index() devuelve el índice de un elemento de la lista
# syntax
lst = ['item1', 'item2']
lst.index(item)
 """
fruits = ['banana', 'orange', 'mango', 'lemon']
print(fruits.index('orange'))   # 1
ages = [22, 19, 24, 25, 26, 24, 25, 24]
print(ages.index(24))           # 2, the first occurrence
# %%

""" 
El método reverse() invierte el orden de una lista.
# syntax
lst = ['item1', 'item2']
lst.reverse()

 """
fruits = ['banana', 'orange', 'mango', 'lemon']
fruits.reverse()
print(fruits) # ['lemon', 'mango', 'orange', 'banana']
ages = [22, 19, 24, 25, 26, 24, 25, 24]
ages.reverse()
print(ages) # [24, 25, 24, 26, 25, 24, 19, 22]
# %%

""" 
Para ordenar listas, podemos usar el método sort()
o la funcione integrada sorted(). El método sort()
reordena los elementos de la lista en orden ascendente
y modifica la lista original. Si un argumento del
método sort(), reverse, es igual a True, ordenará
la lista en orden descendente.
# syntax
lst = ['item1', 'item2']
lst.sort()                # ascending
lst.sort(reverse=True)    # descending
 """
fruits = ['banana', 'orange', 'mango', 'lemon']
fruits.sort()
print(fruits)             # sorted in alphabetical order, ['banana', 'lemon', 'mango', 'orange']
fruits.sort(reverse=True)
print(fruits) # ['orange', 'mango', 'lemon', 'banana']
ages = [22, 19, 24, 25, 26, 24, 25, 24]
ages.sort()
print(ages) #  [19, 22, 24, 24, 24, 25, 25, 26]

ages.sort(reverse=True)
print(ages) #  [26, 25, 25, 24, 24, 24, 22, 19]
# %%

# sorted(): devuelve la lista ordenada sin modificar la lista original
fruits = ['banana', 'orange', 'mango', 'lemon']
print(sorted(fruits))   # ['banana', 'lemon', 'mango', 'orange']
# Reverse order
fruits = ['banana', 'orange', 'mango', 'lemon']
fruits = sorted(fruits,reverse=True)
print(fruits)     # ['orange', 'mango', 'lemon', 'banana']
# %%
