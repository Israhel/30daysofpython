# --- Day 5
# Ejercicios: Level 1

# %%
# 1. Declare an empty list
empty_list = []

# %%
# 2. Declare a list with more than 5 items
lst = [1, 2, 3, 4, 5, 6, 7]

# %%
# 3. Find the length of your list
print(len(lst))

# %%
# 4. Get the first item, the middle item and the last item of the list
print(f'Primer elemento: {lst[0]}')
print(f'Elemento de en medio: {lst[len(lst) // 2]}')
print(f'Ultimo elemento: {lst[-1]}')

# %%
# 5. Declare a list called mixed_data_types, put your(name, age, height, marital status, address)
mixed_data_types = ['Israhel', 3000, 1.85,'Casado con la vida','En algun lugar del mundo']

# %%
# 6. Declare a list variable named it_companies and assign initial values
# Facebook, Google, Microsoft, Apple, IBM, Oracle and Amazon.
it_companies = ['Facebook', 'Google', 'Microsoft', 'Apple', 'IBM', 'Oracle', 'Amazon']

# %%
# 7. Print the list using _print()_
print(it_companies)

# %%
# 8. Print the number of companies in the list
print(len(it_companies))

# %%
# 9. Print the first, middle and last company
print(f'Primer elemento: {it_companies[0]}')
print(f'Elemento de en medio: {it_companies[len(it_companies) // 2]}')
print(f'Ultimo elemento: {it_companies[-1]}')

# %%
# 10. Print the list after modifying one of the companies
it_companies[3] = 'OpenAI'
print(it_companies)

# %%
# 11. Add an IT company to it_companies
it_companies.append('Apple')
print(it_companies)

# %%
# 12. Insert an IT company in the middle of the companies list
middle_list = len(it_companies) // 2
it_companies.insert(middle_list, 'Hyperlink')

# %%
# 13. Change one of the it_companies names to uppercase (IBM excluded!)
it_companies[-2] = it_companies[-2].upper()
print(it_companies)

# %%
# 14. Join the it_companies with a string '#;&nbsp; '
print(f'#; '.join(it_companies))

# %%
# 15. Check if a certain company exists in the it_companies list.
print('OpenAI' in it_companies)

# %%
# 16. Sort the list using sort() method
it_companies.sort()
print(it_companies)

# %%
# 17. Reverse the list in descending order using reverse() method
it_companies.reverse()
print(it_companies)

# %%
# 18. Slice out the first 3 companies from the list
print(it_companies[:3])

# %%
# 19. Slice out the last 3 companies from the list
print(it_companies[-3:])

# %%
# 20. Slice out the middle IT company or companies from the list
middle_list = len(it_companies) // 2
print(it_companies[middle_list])

# %%
# 21. Remove the first IT company from the list
it_companies.pop(0)
print(it_companies)

# %%
# 22. Remove the middle IT company or companies from the list
middle_list = len(it_companies) // 2
it_companies.pop(middle_list)
print(it_companies)

# %%
# 23. Remove the last IT company from the list
it_companies.pop()
print(it_companies)

# %%
# 24. Remove all IT companies from the list
it_companies.clear()
print(it_companies)

# %%
# 25. Destroy the IT companies list
del it_companies
# print(it_companies) # NameError: name 'it_companies' is not defined

# %%
""" 
26. Join the following lists:

    front_end = ['HTML', 'CSS', 'JS', 'React', 'Redux']
    back_end = ['Node','Express', 'MongoDB']
 """
front_end = ['HTML', 'CSS', 'JS', 'React', 'Redux']
back_end = ['Node','Express', 'MongoDB']
lst_join = front_end + back_end
print(lst_join)

# %%
# 27. After joining the lists in question 26. Copy the joined list and assign it
# to a variable full_stack. Then insert Python and SQL after Redux.
full_stack = lst_join.copy()
full_stack.insert(full_stack.index('Redux') + 1, 'SQL')
full_stack.insert(full_stack.index('Redux') + 1, 'Python')
print(lst_join,'\n',full_stack)

### Exercises: Level 2
# %%

# 1. The following is a list of 10 students ages:

ages = [19, 22, 19, 24, 20, 25, 26, 24, 25, 24]
# %%
# - Sort the list and find the min and max age
ages.sort()
print(f'Minimo: {min(ages)}')
print(f'Maximo: {max(ages)}')
# %%
# - Add the min age and the max age again to the list
ages.append(min(ages))
ages.append(max(ages))
# %%
# - Find the median age (one middle item or two middle items divided by two)
ages.sort()
median = (ages[(len(ages) // 2) - 1] + ages[len(ages) // 2]) / 2
print(median)
# %%
# - Find the average age (sum of all items divided by their number )
total = 0
for e in ages:
    total += e
average = total / len(ages)

print(average)
# %%
# - Find the range of the ages (max minus min)
ages_range = max(ages) - min(ages)
print(ages_range)
# %%
# - Compare the value of (min - average) and (max - average), use _abs()_ method
min_avg = abs(min(ages) - average)
max_avg = abs(max(ages) - average)
print(min_avg, max_avg)

# %%
# 2. Find the middle country(ies) in the countries list (countries.py)
import countries
length = len(countries.countries)
mid_ctry = countries.countries[length // 2]
print(mid_ctry)

# %%
# 3. Divide the countries list into two equal lists if it is even if not one more country for the first half.
first_half = countries.countries[:length // 2 + 1]
second_half = countries.countries[length // 2 + 1:]

print(first_half)
print(second_half)

# %%
# 4. ['China', 'Russia', 'USA', 'Finland', 'Sweden', 'Norway', 'Denmark']. Unpack the first three countries and the rest as scandic countries.
ch, ru, us, *scandic = ['China', 'Russia', 'USA', 'Finland', 'Sweden', 'Norway', 'Denmark']
print(ch)
print(ru)
print(us)
print(scandic)
# %%
